# octopi

This is Octopi, a powerful Pacman frontend using Qt libs

https://tintaescura.com/projects/octopi/

https://github.com/aarnt/octopi

https://gitlab.manjaro.org/packages/community/octopi

<br><br>

This PKGBUILD creates:

octopi , octopi-notifier-frameworks , and octopi-notifier-qt5



<br><br>
How to clone this repository:

```
git clone https://gitlab.com/azul4/content/pacman-frontend/octopi/octopi.git
```

<br><br>
Install **octopi-notifier-frameworks** to display notifications in KDE

Install **octopi-notifier-qt5** to display the notifications on the rest of the desktops (install **xfce4-notifyd** for notifications in XFCE)

