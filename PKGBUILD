# Maintainer: Philip Müller <philm[at]manjaro[dot]org>
# Maintainer: Mark Wagie <mark at manjaro dot org>
# Contributor: Rob McCathie <rob[at]manjaro[dot]org>
# Contributor: Alexandre Arnt <arnt[at]manjaro[dot]org>
# Contributor: Roland Singer <roland[at]manjaro[dot]org>
# Contributor: Ramon Buldó <ramon[at]manaro[dot]org>
# Contributor: Tomasz Przybył <fademind[at]manaro[dot]org>

pkgbase=octopi
pkgname=('octopi'
         'octopi-notifier-qt5'
         'octopi-notifier-frameworks'
)
pkgver=0.14.0
pkgrel=2.1
arch=('x86_64')
url="https://tintaescura.com/projects/octopi"
license=('GPL2')
depends=('alpm_octopi_utils' 'knotifications' 'qtermwidget')
makedepends=('qt5-tools')
source=("${pkgbase}-${pkgver}.tar.gz::https://github.com/aarnt/octopi/archive/v${pkgver}.tar.gz")
sha256sums=('9b548661807fe8eecc20726a4aefa25658c4ce1bf1bc8f51b54829809d76f12d')

_subdirs=(
  helper
  notifier
  notifier-frameworks
  repoeditor
  cachecleaner
  sudo
)

prepare() {
  cd "${pkgbase}-${pkgver}"
  cp -r notifier notifier-qt5
  cp -r notifier notifier-frameworks
  sed -i 's|#KSTATUS|KSTATUS|' notifier-frameworks/octopi-notifier.pro

  cp resources/images/octopi_green.png resources/images/octopi.png
}

build() {
  cd "${pkgbase}-${pkgver}"
  qmake-qt5 \
    PREFIX=/usr \
    QMAKE_CFLAGS="${CFLAGS}" \
    QMAKE_CXXFLAGS="${CXXFLAGS}" \
    QMAKE_LFLAGS="${LDFLAGS}"
  make

  for _subdir in ${_subdirs[@]}; do
    pushd ${_subdir}
    qmake-qt5 \
    PREFIX=/usr \
    QMAKE_CFLAGS="${CFLAGS}" \
    QMAKE_CXXFLAGS="${CXXFLAGS}" \
    QMAKE_LFLAGS="${LDFLAGS}"
    make
    popd
  done
}

package_octopi() {
  pkgdesc="A powerful Pacman frontend using Qt libs"
  depends=('alpm_octopi_utils' 'pkgfile' 'qtermwidget')
  optdepends=('octopi-notifier-qt5: Notifier for Octopi using Qt5 libs'
              'octopi-notifier-frameworks: Notifier for Octopi with Knotifications support'
#              'pacaur: for AUR support'
#              'paru: for AUR support'
#              'pikaur: for AUR support'
#              'trizen: for AUR support'
              'yay: for AUR support'
              'pacmanlogviewer: to view pacman log files'
              'opendoas: privilege elevation'
              'sudo: privilege elevation')
  conflicts=('octopi-qt5' 'octopi-pacmanhelper' 'octopi-repoeditor' 'octopi-cachecleaner')
  replaces=('octopi-qt5' 'octopi-pacmanhelper' 'octopi-repoeditor' 'octopi-cachecleaner')

  cd "${pkgbase}-${pkgver}"
  make INSTALL_ROOT="${pkgdir}" install

  for folder in helper repoeditor cachecleaner sudo; do
    make -C ${folder} INSTALL_ROOT="${pkgdir}" install
  done
}

package_octopi-notifier-qt5() {
  pkgdesc="Notifier for Octopi using Qt5 libs"
  depends=('octopi')
  provides=("octopi-notifier=${pkgver}")
  conflicts=('octopi-notifier')
  replaces=('octopi-qt5-notifier' 'octopi-notifier-qt4')

  cd "${pkgbase}-${pkgver}"
  make -C notifier INSTALL_ROOT="${pkgdir}" install
}

package_octopi-notifier-frameworks() {
  pkgdesc="Notifier for Octopi with Knotifications support"
  depends=('octopi' 'knotifications')
  provides=("octopi-notifier=${pkgver}")
  conflicts=('octopi-notifier')

  cd "${pkgbase}-${pkgver}"
  make -C notifier-frameworks INSTALL_ROOT="${pkgdir}" install
}
